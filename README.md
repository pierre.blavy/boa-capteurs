# Objectifs
Cette page a pour but de collecter les besoins en terme de suivi des conditions d'élevage sur le site inrae de Tours. Le contenu de cette page est **public**.


## Que faut-il mesurer ?

|Importance|Mesure|Précision|Qui est intéréssé|Notes|
|---|---|---|---|---|
|Important|T° : Temperature_sol|1°C|Elodie Guettier|Température près du sol (là où vivent les poulets)|
|Important|H : Humidité|10%??|Elodie Guettier|Humidité. Où mesurer?|
|Important|L : Lumière|on/off|Elodie Guettier|Lampes alumées ou étintes|
|Si facile|NH3||Elodie Guettier|Sous réserve qu'il existe des capteurs adaptés. Une mesure grossière pour générer une alarme ventilation suffit|


## Retour des données
* Important : On veut un retour en léger différé (ex tt les 15 min) sur un serveur centralisé pour pouvoir détecter rapidement les problèmes et générer des alertes
* Important : On veut un retour en temps réel sur place, pour voir si les capteurs marchent => Ecran.

## Où mesurer ?
* L'objectif est d'avoir une mesure par cellule pour chaque élevage 
* ==> On veut un système facilement déployable partout, qui ne nécessite pas (ou peu) de maintenance
* ==> On veut un truc pas trop cher.

## Que stocker ?
* Faut il avoir un backup local des données (ex carte SD) au cas ou le réseau plante ?. Si oui RTC + carte SD

# Contraintes
* Les élevages sont nettoyés à grande eau. Système étanche ou amovible.
* Amiante => Pas de trous dans les murs
* Métal ou plastique => pas de bois.

## Etat du réseau
* **Important ** : Quel est l'état du réseau internet dans les élevage ? 
* A t'on des cables ethernet dans chaque batiment ?
* A t'on des cables ethernet dans chaque cellule ?


# Existants
## BOA
* Il y a des appareils mobiles qui mesurent la température et l'humidité. Ils fonctionnent sur batterie, et on peut les déplacer un peu partout. Ils permettent d'avoir un suivi des conditions d'élevage, mais ils ne sont pas connectés et à ma connaissance ils ne permettent pas un retour temps réel des données.


## CATI SICPA
* Sophie Normant est en charge du projet Environnement dans son ensemble. (contactée, j'attends son retour)
* Jean-François Bompa pour la partie capteurs et remontée de données.
* Thierry Heirman pour la partie InfluxDB et tableaux de bord.
* BOA va être équipé d'une antenne LoRA, supportée par le cati. ** C'est la solition privilégiée **



# Composants
|Mesure|BUS|Composant|prix|Datasheet|
|---|---|---|---|---|
|T°, P, H, gaz(1) |I2C|BME680       |3.00€| https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680/ |
|T°               |ADC|LM35         |0.50€| https://www.ti.com/lit/ds/symlink/lm35.pdf |
|L                |ADC|GL55xx (2)   |0.20€| https://www.kth.se/social/files/54ef17dbf27654753f437c56/GL5537.pdf |
|L                |I2C|TSL25911 (3) |6.00€| https://cdn-shop.adafruit.com/datasheets/TSL25911_Datasheet_EN_v1.pdf |

(1) Renvoie uniquement un index de la qualité de l'air, qui est une mesure combinée de plusieurs gaz. Je ne sais pas si cette mesure est pertinente pour un élevage. Elle est aussi relativement peu précise (Sensor to sensor deviation +- 15%). C'est peut être OK pour une alarme, mais certainement pas pour faire des mesures. Le capteur permet aussi de mesurer la pression atmosphérique.

(2) This sensors are not acurate, but are good enough for a ON/OFF value for light. They have to be mounted as a tension divisor with a classical resistor. 

(3) This sensor is much more precise than a GL55xx photoresistor, and has a wider dynalic range (188µLux to 88kLux) and mesures in about 500 to 900 nm range. This sensor has support here : https://www.arduino.cc/reference/en/libraries/adafruit-tsl2591-library/.

## Resultats de tests
Développer l'électronique autour de capteurs courants est assez facile, par contre ces derniers offrent des performances aléatoires qui ne suivent pas du tout ce qu'il y a écrit dans les datasheets. J'ai par exemple pu voir des courbes de températures avec 2°C de différence sur des capteurs certifiés précis au centième de degré. Ce sont donc des solutions à utiliser en dernier recours, et à bien tester avant de les déployer sur le terrain.



# Conclusion
L'utilisation de la technomogie LoRA semble bien adaptée à la réalisation de diverses mesures et le large catalogue d'appareil existants chez divers fournisseur permet de mettre en place des capteurs sans se prendre la tête avec du développement en interne. Le site de BOA est équipée d'une antenne LoRA et le CATI SICPA offre du support pour cette technologie. Sauf bonne raison, il faut passer par là.





## Doc
* https://www.seeedstudio.com/blog/2020/04/07/how-to-pick-the-best-temperature-and-humidity-sensor-for-your-arduino-project/
* WARNING : after testing the DHT11 sensors have an acuracy for humidity much worse than what's written in the datasheet.
* ESP : urlencode/urldecode https://circuits4you.com/2019/03/21/esp8266-url-encode-decode-example/
* ESP : get query param : https://techtutorialsx.com/2016/10/22/esp8266-webserver-getting-query-parameters/
* ESP32 Atmoscan : https://randomnerdtutorials.com/esp32-influxdb/
* ESP32 influxDB https://randomnerdtutorials.com/esp32-influxdb/
* ESP : ESPnow https://randomnerdtutorials.com/esp-now-auto-pairing-esp32-esp8266/
* ESP : ESPnow encrypt https://randomnerdtutorials.com/esp32-esp-now-encrypted-messages/
* ESP32 protocols https://randomnerdtutorials.com/esp32-wireless-communication-protocols/


